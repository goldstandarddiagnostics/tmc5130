/*
 * TMC5130.c
 *
 *  Created on: 03.07.2017
 *      Author: LK
 */

#include "TMC5130.h"
#include "LL_spi.h"
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "TMC5130_Utils.h"

#define TMC5130_SPI_MODE spmMode3

struct TMC5130_private_t
{
	bool _IsInShakePositionA;
	uint8_t _stopSwitchID;
	bool _stopSwitchInverted;
	int32_t  _ShakePositionA;
	int32_t  _ShakePositionB;
	int32_t _targetShakeSpeedInms;
	int32_t _lastStartTimeInms;
	int32_t _lastshakespeed;
	int32_t _targetMoveTimeInms;
	int32_t _IHOLD_IRUN_NORMAL;
	int32_t _IHOLD_IRUN_SHAKER;
	uint16_t _Prev_SG_Result;
	float _shakerIValue;
};

// Writes (x1 << 24) | (x2 << 16) | (x3 << 8) | x4 to the given address
void tmc5130_writeDatagram(struct TMC5130_t *tmc5130, uint8_t address, uint8_t x1, uint8_t x2, uint8_t x3, uint8_t x4)
{
	uint8_t data[5] = { address | TMC5130_WRITE_BIT, x1, x2, x3, x4 };
	tmc5130->SPIReadWriteCallback(&data[0], 5, tmc5130->ChipID, TMC5130_SPI_MODE);
	int32_t value = (x1 << 24) | (x2 << 16) | (x3 << 8) | x4;

	// Write to the shadow register and mark the register dirty
	address = TMC_ADDRESS(address);
	tmc5130->config->shadowRegister[address] = value;
	tmc5130->registerAccess[address] |= TMC_ACCESS_DIRTY;
}

// Write an integer to the given address
void tmc5130_writeInt(struct TMC5130_t *tmc5130, uint8_t address, int32_t value)
{
	tmc5130_writeDatagram(tmc5130, address, BYTE(value, 3), BYTE(value, 2), BYTE(value, 1), BYTE(value, 0));
}

// Read an integer from the given address
int32_t tmc5130_readInt(struct TMC5130_t *tmc5130, uint8_t address)
{
	address = TMC_ADDRESS(address);

	// register not readable -> shadow register copy
	if(!TMC_IS_READABLE(tmc5130->registerAccess[address]))
		return tmc5130->config->shadowRegister[address];

	uint8_t data[5] = { 0, 0, 0, 0, 0 };

	data[0] = address;
  	tmc5130->SPIReadWriteCallback(&data[0], 5, tmc5130->ChipID, TMC5130_SPI_MODE);

	data[0] = address;
  	tmc5130->SPIReadWriteCallback(&data[0], 5, tmc5130->ChipID, TMC5130_SPI_MODE);

	return (data[1] << 24) | (data[2] << 16) | (data[3] << 8) | data[4];
}

// Initialize a TMC5130 IC.
// This function requires:
//     - tmc5130: The pointer to a TMC5130TypeDef struct, which represents one IC
//     - config: A ConfigurationTypeDef struct, which will be used by the IC
//     - registerResetState: An int32_t array with 128 elements. This holds the values to be used for a reset.
void tmc5130_init(struct TMC5130_t *tmc5130,  ConfigurationTypeDef *config, const int32_t *registerResetState)
{
	tmc5130->LastStoppedPosition  = 0;
	tmc5130->MotorState  = msIdle;
	tmc5130->config               = config;
	tmc5130->config->callback     = NULL;
	tmc5130->config->configIndex  = 0;
	tmc5130->config->state        = CONFIG_READY;

	size_t i;
	for(i = 0; i < TMC5130_REGISTER_COUNT; i++)
	{
		tmc5130->registerAccess[i]      = tmc5130_defaultRegisterAccess[i];
		tmc5130->registerResetState[i]  = registerResetState[i];
	}
	tmc5130->_private = malloc(sizeof (struct TMC5130_private_t));
	((struct TMC5130_private_t*)tmc5130->_private)->_IsInShakePositionA = false;
}

// Fill the shadow registers of hardware preset non-readable registers
// Only needed if you want to 'read' those registers e.g to display the value
// in the TMCL IDE register browser
void tmc5130_fillShadowRegisters(struct TMC5130_t *tmc5130)
{
	// Check if we have constants defined
	if(ARRAY_SIZE(tmc5130_RegisterConstants) == 0)
		return;

	size_t i, j;
	for(i = 0, j = 0; i < TMC5130_REGISTER_COUNT; i++)
	{
		// We only need to worry about hardware preset, write-only registers
		// that have not yet been written (no dirty bit) here.
		if(tmc5130->registerAccess[i] != TMC_ACCESS_W_PRESET)
			continue;

		// Search the constant list for the current address. With the constant
		// list being sorted in ascended order, we can walk through the list
		// until the entry with an address equal or greater than i
		while(j < ARRAY_SIZE(tmc5130_RegisterConstants) && (tmc5130_RegisterConstants[j].address < i))
			j++;

		// Abort when we reach the end of the constant list
		if (j == ARRAY_SIZE(tmc5130_RegisterConstants))
			break;

		// If we have an entry for our current address, write the constant
		if(tmc5130_RegisterConstants[j].address == i)
		{
			tmc5130->config->shadowRegister[i] = tmc5130_RegisterConstants[j].value;
		}
	}
}

// Reset the TMC5130.
uint8_t tmc5130_reset(struct TMC5130_t *tmc5130)
{
	if(tmc5130->config->state != CONFIG_READY)
		return false;

	// Reset the dirty bits and wipe the shadow registers
	size_t i;
	for(i = 0; i < TMC5130_REGISTER_COUNT; i++)
	{
		tmc5130->registerAccess[i] &= ~TMC_ACCESS_DIRTY;
		tmc5130->config->shadowRegister[i] = 0;
	}

	tmc5130->config->state        = CONFIG_RESET;
	tmc5130->config->configIndex  = 0;

	return true;
}

// Restore the TMC5130 to the state stored in the shadow registers.
// This can be used to recover the IC configuration after a VM power loss.
uint8_t tmc5130_restore(struct TMC5130_t *tmc5130)
{
	if(tmc5130->config->state != CONFIG_READY)
		return false;

	tmc5130->config->state        = CONFIG_RESTORE;
	tmc5130->config->configIndex  = 0;

	return true;
}

// Change the values the IC will be configured with when performing a reset.
void tmc5130_setRegisterResetState(struct TMC5130_t *tmc5130, const int32_t *resetState)
{
	size_t i;
	for(i = 0; i < TMC5130_REGISTER_COUNT; i++)
	{
		tmc5130->registerResetState[i] = resetState[i];
	}
}

// Register a function to be called after completion of the configuration mechanism
void tmc5130_setCallback(struct TMC5130_t *tmc5130, tmc5130_callback callback)
{
	tmc5130->config->callback = (tmc_callback_config) callback;
}

// Helper function: Configure the next register.
static void writeConfiguration(struct TMC5130_t *tmc5130)
{
	uint8_t *ptr = &tmc5130->config->configIndex;
	const int32_t *settings;

	if(tmc5130->config->state == CONFIG_RESTORE)
	{
		settings = tmc5130->config->shadowRegister;
		// Find the next restorable register
		while((*ptr < TMC5130_REGISTER_COUNT) && !TMC_IS_RESTORABLE(tmc5130->registerAccess[*ptr]))
		{
			(*ptr)++;
		}
	}
	else
	{
		settings = tmc5130->registerResetState;
		// Find the next resettable register
		while((*ptr < TMC5130_REGISTER_COUNT) && !TMC_IS_RESETTABLE(tmc5130->registerAccess[*ptr]))
		{
			(*ptr)++;
		}
	}

	if(*ptr < TMC5130_REGISTER_COUNT)
	{
		tmc5130_writeInt(tmc5130, *ptr, settings[*ptr]);
		(*ptr)++;
	}
	else // Finished configuration
	{
		if(tmc5130->config->callback)
		{
			((tmc5130_callback)tmc5130->config->callback)(tmc5130, tmc5130->config->state);
		}

		tmc5130->config->state = CONFIG_READY;
	}
}

// Call this periodically
void tmc5130_periodicJob(struct TMC5130_t* tmc5130, uint32_t ticks)
{
	if(tmc5130->config->state != CONFIG_READY)
	{
		writeConfiguration(tmc5130);
		return;
	}
	volatile int32_t xactual = tmc5130_readInt(tmc5130, TMC5130_XACTUAL);
	volatile int32_t rampstatus = tmc5130_readInt(tmc5130, TMC5130_RAMPSTAT);
	volatile MotorState_t tempMotorState = tmc5130->MotorState;
	struct TMC5130_private_t* private = tmc5130->_private;
	bool switchState = false;
	switch (tempMotorState)
	{
	case msShaking:
		if(rampstatus & TMC5130_RS_VZERO_MASK) //check if speed is zero.
		{
			volatile int32_t timeDelta = ticks - private->_lastStartTimeInms;
			if (timeDelta > 1000) timeDelta = 1;
			float timeError = timeDelta - private->_targetShakeSpeedInms;
			int32_t speedAdjust = (int32_t)(timeError * private->_shakerIValue);
			if (abs(speedAdjust) > 2)
			{
				int newspeed = private->_lastshakespeed + speedAdjust;
				if (newspeed > 0)
				{
					tmc5130_writeInt(tmc5130, TMC5130_VMAX, newspeed);
					private->_lastshakespeed = newspeed;
				}
			}

			if(private->_IsInShakePositionA)
			{
				tmc5130_writeInt(tmc5130, TMC5130_XTARGET, private->_ShakePositionB);
				private->_IsInShakePositionA = false;
			}
			else
			{
				tmc5130_writeInt(tmc5130, TMC5130_XTARGET, private->_ShakePositionA);
				private->_IsInShakePositionA = true;
			}
			private->_lastStartTimeInms = ticks;
			tmc5130->MotorState = msShakingRamp;
		}
		break;
	case msShakingRamp:
		if(!(rampstatus & TMC5130_RS_VZERO_MASK)) //check if speed is NOT zero
		{
			tmc5130->MotorState = msShaking;
			private->_lastStartTimeInms = ticks;
		}
		break;
	case msIdle:
		break;
	case msStopped:
		tmc5130_writeInt(tmc5130, TMC5130_TCOOLTHRS, 0);
		tmc5130->MotorState = msIdle;
		tmc5130->LastStoppedPosition = xactual;
		tmc5130->IntReason = irStopped;
		if(tmc5130->SetInterruptCallback) //check if callback was assigned
		{
			tmc5130->SetInterruptCallback(tmc5130, tmc5130->ChipID);
		}
		break;
	case msStalled:
		tmc5130->IntReason = irStalled;
		if(tmc5130->SetInterruptCallback) //check if callback was assigned
		{
			tmc5130->SetInterruptCallback(tmc5130, tmc5130->ChipID);
		}
		tmc5130->MotorState = msIdle;
		break;
	case msConstantVelocity:

		break;
	case msConstantVelocityRampingDown:
		if(rampstatus & TMC5130_RS_VZERO_MASK) //check if speed is zero
		{
			tmc5130->MotorState = msStopped;
		}
		break;
	case msTrajectory:
		if(rampstatus & TMC5130_RS_VZERO_MASK) //check if speed is zero and that position is reached.
		{
			int32_t delta = tmc5130->LastTargetPosition - xactual;
			if(abs(delta) < 10)
			{
				tmc5130_writeInt(tmc5130, TMC5130_XTARGET, xactual);
				tmc5130->MotorState = msStopped;
			}
		}
		break;
	case msShakingHoming:
		if(rampstatus & TMC5130_RS_VZERO_MASK) //check if speed is zero.
		{
			tmc5130_writeInt(tmc5130, TMC5130_VMAX, 4000);
			tmc5130_writeInt(tmc5130, TMC5130_XTARGET, tmc5130->LastStoppedPosition);
			tmc5130->MotorState = msShakingStopping;
		}
		break;
	case msShakingStopping:
		if(rampstatus & TMC5130_RS_VZERO_MASK) //check if speed is zero.
		{
			tmc5130->MotorState = msIdle;
			tmc5130->IntReason = irStopped;
			if(tmc5130->SetInterruptCallback) //check if callback was assigned
			{
				tmc5130->SetInterruptCallback(tmc5130, tmc5130->ChipID);
			}
		}
		break;
	case msTimedConstantVelocityRampUp:
		if(rampstatus & TMC5130_RS_VELREACHED_MASK) //check if speed is reached.
		{
			tmc5130->MotorState = msTimedConstantVelocity;
			int32_t drv_stat = tmc5130_readInt(tmc5130, TMC5130_DRVSTATUS);
			private->_Prev_SG_Result = (drv_stat & TMC5130_SG_RESULT_MASK);
		}
		break;
	case msTimedConstantVelocity:
		if((ticks - private->_lastStartTimeInms) >= private->_targetMoveTimeInms)
		{
			tmc5130_writeInt(tmc5130, TMC5130_AMAX, 65535);
			tmc5130_writeInt(tmc5130, TMC5130_VMAX,0);
			tmc5130->MotorState = msStopped;
		}
		else
		{
			int32_t drv_stat = tmc5130_readInt(tmc5130, TMC5130_DRVSTATUS);
			uint16_t sg_result = (drv_stat & TMC5130_SG_RESULT_MASK);
			uint16_t sg_result_prev = private->_Prev_SG_Result;
			if (sg_result < (sg_result_prev / tmc5130->StallguardDivider))
			{
				tmc5130_writeInt(tmc5130, TMC5130_AMAX, 65535);
				tmc5130_writeInt(tmc5130, TMC5130_VMAX,0);
				tmc5130->MotorState = msStopped;
			}
			else
			{
				if (sg_result > sg_result_prev)
				{
					private->_Prev_SG_Result = sg_result;
				}
			}
		}
		break;
	case msConstantVelocityUntilSwitch:
        switch (private->_stopSwitchID) {
            case 0:
                switchState = ((rampstatus & STOPSWITCH0MASK) != 0);
                break;
            case 1:
                switchState = ((rampstatus & STOPSWITCH1MASK) != 0);
                break;
            case 254: // Custom Callback!
                switchState = tmc5130->GetCustomHomeSwitchActuatedCallback(tmc5130);
                break;
        }
		if(private->_stopSwitchInverted)
		{
			if(!switchState)
			{
				tmc5130_writeInt(tmc5130, TMC5130_AMAX, 65535);
				tmc5130_writeInt(tmc5130, TMC5130_VMAX, 0);
				tmc5130->MotorState = msStopped;
			}
		}
		else
		{
			if(switchState)
			{
				tmc5130_writeInt(tmc5130, TMC5130_AMAX, 65535);
				tmc5130_writeInt(tmc5130, TMC5130_VMAX, 0);
				tmc5130->MotorState = msStopped;
			}
		}
		break;
	}
}

void tmc5130_StartTimedConstantVelocity(struct TMC5130_t *instance, uint32_t acc, int32_t velocity, uint32_t timeInms, uint32_t ticks)
{
	struct TMC5130_private_t* private = instance->_private;
	private->_lastStartTimeInms = ticks;
	private->_targetMoveTimeInms = timeInms;
	// Set Current for Motor
	tmc5130_writeInt(instance, TMC5130_IHOLD_IRUN, private->_IHOLD_IRUN_NORMAL);
	// Set absolute velocity
	tmc5130_writeInt(instance, TMC5130_VMAX, SpeedPPSToMotorUnits(abs(velocity)));
	tmc5130_writeInt(instance, TMC5130_AMAX, AccDecPPSToMotorUnits(acc));
	// Set StallGuard On
	tmc5130_writeInt(instance, TMC5130_TCOOLTHRS, 1048570);
	// Set direction
	tmc5130_writeInt(instance, TMC5130_RAMPMODE, (velocity >= 0) ? TMC5130_MODE_VELPOS : TMC5130_MODE_VELNEG);
	instance->MotorState = msTimedConstantVelocityRampUp;
}

void tmc5130_StartMoveUntilSwitch(struct TMC5130_t *instance, uint8_t switchId, bool polarity, uint32_t acc, int32_t velocity)
{
    if(switchId == 254) //check if callback was assigned
    {
        if (!(instance->GetCustomHomeSwitchActuatedCallback))
        {
            return;
        }
    }
    struct TMC5130_private_t* private = instance->_private;
    // Set Current for Motor
	tmc5130_writeInt(instance, TMC5130_IHOLD_IRUN, private->_IHOLD_IRUN_NORMAL);
	private->_stopSwitchID = switchId;
	private->_stopSwitchInverted = !polarity;
	// Set absolute velocity
	tmc5130_writeInt(instance, TMC5130_VMAX, SpeedPPSToMotorUnits(abs(velocity)));
	tmc5130_writeInt(instance, TMC5130_AMAX, AccDecPPSToMotorUnits(acc));
	// Set direction
	tmc5130_writeInt(instance, TMC5130_RAMPMODE, (velocity >= 0) ? TMC5130_MODE_VELPOS : TMC5130_MODE_VELNEG);
	instance->MotorState = msConstantVelocityUntilSwitch;
}

void tmc5130_startShaking(struct TMC5130_t *instance, int32_t initialshakespeed, int32_t shakeAmplitude, int32_t targetshaketime, float shakerIValue, uint32_t ticks)
{
	struct TMC5130_private_t* private = instance->_private;
	private->_lastStartTimeInms = ticks;
	private->_IsInShakePositionA = false;
	private->_lastshakespeed = initialshakespeed; //init to a good value
	private->_targetShakeSpeedInms = targetshaketime / 2;
	private->_shakerIValue = shakerIValue;
	int32_t halfShakeAmplitude = shakeAmplitude / 2;
	private->_ShakePositionA = instance->LastStoppedPosition + halfShakeAmplitude;
	private->_ShakePositionB = instance->LastStoppedPosition - halfShakeAmplitude;
	tmc5130_writeInt(instance, TMC5130_IHOLD_IRUN, private->_IHOLD_IRUN_SHAKER);
	tmc5130_writeInt(instance, TMC5130_AMAX, 1024000); //High acc and dec for shaker
	tmc5130_writeInt(instance, TMC5130_DMAX, 1024000); //High acc and dec for shaker
	tmc5130_writeInt(instance, TMC5130_VMAX, SpeedPPSToMotorUnits(initialshakespeed));
	tmc5130_writeInt(instance, TMC5130_XACTUAL, instance->LastStoppedPosition);
	instance->MotorState = msShaking;
}

void tmc5130_stopShaking(struct TMC5130_t *instance)
{
	instance->MotorState = msShakingHoming;
}

// Stop moving
void tmc5130_stop(struct TMC5130_t *instance)
{
	tmc5130_writeInt(instance, TMC5130_VMAX, 0);
    instance->MotorState = msStopped;
}

// Move to a specified position with a given speed, acceleration and de-acceleration
void tmc5130_moveTo(struct TMC5130_t *instance, int32_t position, uint32_t acc, uint32_t speedMax, uint32_t dec)
{
    // Set Current for Motor
	struct TMC5130_private_t* private = instance->_private;
	tmc5130_writeInt(instance, TMC5130_IHOLD_IRUN, private->_IHOLD_IRUN_NORMAL);
	tmc5130_writeInt(instance, TMC5130_RAMPMODE, TMC5130_MODE_POSITION);
	tmc5130_writeInt(instance, TMC5130_VMAX, SpeedPPSToMotorUnits(speedMax));
	tmc5130_writeInt(instance, TMC5130_AMAX, AccDecPPSToMotorUnits(acc));
	tmc5130_writeInt(instance, TMC5130_DMAX, AccDecPPSToMotorUnits(dec));
    tmc5130_writeInt(instance, TMC5130_XTARGET, position);
	instance->LastTargetPosition = position;
	instance->MotorState = msTrajectory;
}

/// Sets the Idle current scaling factor
/// \param instance a pointer to the instance struct
/// \param idleCurrent a value between 0 and 31, where 31 is full scale current
/// \param moveCurrent a value between 0 and 31, where 31 is full scale current
void tmc5130_setDigitalCurrentControls(struct TMC5130_t *instance, uint8 idleCurrent, uint8_t moveCurrent, uint8_t moveCurrentShaker)
{
	struct TMC5130_private_t* private = instance->_private;
 	int32_t value = 4 << 16; //4 is a good IHOLDDELAY value for most cases, this will do a smooth transition from move to idle current
	value |= (moveCurrent << 8);
	value |= idleCurrent;
	private->_IHOLD_IRUN_NORMAL = value;
	tmc5130_writeInt(instance, TMC5130_IHOLD_IRUN, value);

	value = 4 << 16; //4 is a good IHOLDDELAY value for most cases, this will do a smooth transition from move to idle current
	value |= (moveCurrentShaker << 8);
	value |= idleCurrent;
	private->_IHOLD_IRUN_SHAKER = value;
}

#define TWO_TO_THE_EIGHTTEENTH 262144.0

// Sets the Idle current scaling factor
/// \param instance a pointer to the instance struct
/// \param powerDownInmS sets the delay time after stand still of the motor to motor current power down. in mS.
void tmc5130_setDigitalCurrentPowerDown(struct TMC5130_t *instance, uint16_t powerDownInmS)
{
	int32_t value = round(mSToMotorUnits(powerDownInmS) / TWO_TO_THE_EIGHTTEENTH);
	instance->registerResetState[TMC5130_TPOWERDOWN] = value;
	tmc5130_writeInt(instance, TMC5130_TPOWERDOWN, value);
}

