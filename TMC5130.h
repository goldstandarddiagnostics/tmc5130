/*
 * TMC5130.h
 *
 *  Created on: 03.07.2017
 *      Author: LK
 */

#ifndef TMC_IC_TMC5130_H_
#define TMC_IC_TMC5130_H_

#include "LL_spi.h"
#include "API_Header.h"
#include "TMC5130_Register.h"
#include "TMC5130_Constants.h"
#include "TMC5130_Fields.h"

// Helper macros
#define TMC5130_FIELD_READ(tdef, address, mask, shift) \
	FIELD_GET(tmc5130_readInt(tdef, address), mask, shift)
#define TMC5130_FIELD_WRITE(tdef, address, mask, shift, value) \
	(tmc5130_writeInt(tdef, address, FIELD_SET(tmc5130_readInt(tdef, address), mask, shift, value)))

#ifdef __cplusplus
extern "C"
{
#endif

// Forward Delarations
struct TMC5130_t;

// Typedefs
typedef void (* SetMotorCurrentCallback_t)(struct TMC5130_t* sender, uint16_t motorCurrentInmA, uint8_t chipID);
typedef void (* SetInterruptCallback_t)(struct TMC5130_t* sender, uint8_t chipID);
typedef bool (* GetCustomHomeSwitchActuatedCallback_t)(struct TMC5130_t* sender);

typedef enum
{
	msIdle, msStopped, msConstantVelocity, msConstantVelocityRampingDown, msTrajectory,
	msStalled, msShaking, msShakingRamp, msShakingHoming, msShakingStopping,
	msTimedConstantVelocity, msConstantVelocityUntilSwitch, msTimedConstantVelocityRampUp
} MotorState_t;

typedef enum
{
	irNone, irStopped, irStalled
} MotorIntReason_t;

struct TMC5130_t
{
	void* _private;
	ConfigurationTypeDef* config;
	int32_t registerResetState[TMC5130_REGISTER_COUNT];
	uint8_t registerAccess[TMC5130_REGISTER_COUNT];
	LL_SPIMaster_ReadWriteMethod_t SPIReadWriteCallback;
	SetInterruptCallback_t SetInterruptCallback;
    GetCustomHomeSwitchActuatedCallback_t GetCustomHomeSwitchActuatedCallback;
	uint8_t ChipID;
	uint16_t IdleMotorCurrent;
	uint16_t MoveMotorCurrent;
	uint16_t ShakeMotorCurrent;
	MotorIntReason_t IntReason;
	MotorState_t MotorState;
	int32_t LastStoppedPosition;
	int32_t LastTargetPosition;
	uint16 MaxMotorCurrent;
	float StallguardDivider;
};

typedef void (* tmc5130_callback)(struct TMC5130_t*, ConfigState);

static const int STOPSWITCH0MASK = 1;
static const int STOPSWITCH1MASK = 2;

// Default Register values
#define R10 0x00071703  // IHOLD_IRUN
#define R3A 0x00010000  // ENC_CONST
#define R6C 0x000101D5  // CHOPCONF

static const int32_t tmc5130_defaultRegisterResetState[TMC5130_REGISTER_COUNT] =
  {
//	0,   1,   2,   3,   4,   5,   6,   7,   8,   9,   A,   B,   C,   D,   E,   F
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0x00 - 0x0F
	R10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0x10 - 0x1F
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0x20 - 0x2F
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, R3A, 0, 0, 0, 0, 0, // 0x30 - 0x3F
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0x40 - 0x4F
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0x50 - 0x5F
	N_A, N_A, N_A, N_A, N_A, N_A, N_A, N_A, N_A, N_A, 0, 0, R6C, 0, 0, 0, // 0x60 - 0x6F
	N_A, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0x70 - 0x7F
  };

// Register access permissions:
//   0x00: none (reserved)
//   0x01: read
//   0x02: write
//   0x03: read/write
//   0x13: read/write, seperate functions/values for reading or writing
//   0x21: read, flag register (read to clear)
//   0x42: write, has hardware presets on reset
static const uint8_t tmc5130_defaultRegisterAccess[TMC5130_REGISTER_COUNT] =
  {
//  0     1     2     3     4     5     6     7     8     9     A     B     C     D     E     F
	0x03, 0x21, 0x01, 0x02, 0x13, 0x02, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____, // 0x00 - 0x0F
	0x02, 0x02, 0x01, 0x02, 0x02, 0x02, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____, // 0x10 - 0x1F
	0x03, 0x03, 0x01, 0x06, 0x02, 0x02, 0x02, 0x02, 0x02, ____, 0x02, 0x06, 0x02, 0x03, ____, ____, // 0x20 - 0x2F
	____, ____, ____, 0x02, 0x03, 0x21, 0x01, ____, 0x03, 0x03, 0x02, 0x21, 0x01, ____, ____, ____, // 0x30 - 0x3F
	____, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____, // 0x40 - 0x4F
	____, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____, // 0x50 - 0x5F
	0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x01, 0x01, 0x03, 0x02, 0x02, 0x01, // 0x60 - 0x6F
	0x42, 0x01, 0x02, 0x01, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____, ____  // 0x70 - 0x7F
  };

// Register constants (only required for 0x42 registers, since we do not have
// any way to find out the content but want to hold the actual value in the
// shadow register so an application (i.e. the TMCL IDE) can still display
// the values. This only works when the register content is constant.
static const TMCRegisterConstant tmc5130_RegisterConstants[] =
  {        // Use ascending addresses!
	{ 0x60, 0xAAAAB554 }, // MSLUT[0]
	{ 0x61, 0x4A9554AA }, // MSLUT[1]
	{ 0x62, 0x24492929 }, // MSLUT[2]
	{ 0x63, 0x10104222 }, // MSLUT[3]
	{ 0x64, 0xFBFFFFFF }, // MSLUT[4]
	{ 0x65, 0xB5BB777D }, // MSLUT[5]
	{ 0x66, 0x49295556 }, // MSLUT[6]
	{ 0x67, 0x00404222 }, // MSLUT[7]
	{ 0x68, 0xFFFF8056 }, // MSLUTSEL
	{ 0x69, 0x00F70000 }, // MSLUTSTART
	{ 0x70, 0x00050480 }  // PWMCONF
  };

// API Functions
// All functions act on one IC identified by the TMC5130TypeDef pointer

void tmc5130_writeDatagram(struct TMC5130_t* tmc5130, uint8_t address, uint8_t x1, uint8_t x2, uint8_t x3, uint8_t x4);
void tmc5130_writeInt(struct TMC5130_t* tmc5130, uint8_t address, int32_t value);
int32_t tmc5130_readInt(struct TMC5130_t* tmc5130, uint8_t address);

void tmc5130_init(struct TMC5130_t* tmc5130, ConfigurationTypeDef* config, const int32_t* registerResetState);
void tmc5130_fillShadowRegisters(struct TMC5130_t* tmc5130);
uint8_t tmc5130_reset(struct TMC5130_t* tmc5130);
uint8_t tmc5130_restore(struct TMC5130_t* tmc5130);
void tmc5130_setRegisterResetState(struct TMC5130_t* tmc5130, const int32_t* resetState);
void tmc5130_setCallback(struct TMC5130_t* tmc5130, tmc5130_callback callback);
void tmc5130_periodicJob(struct TMC5130_t* tmc5130, uint32_t ticks);

/// Moves the motor for a set period of time
/// \param instance a pointer to the instance struct
/// \param acc Unsigned Acceleration Value
/// \param velocity Unsigned Velocity Max. Range +-(2^23-512)
/// \param timeInms Time in ms the motor should move
/// \param ticks Current tick value from the Systick timer or an equivalent 1ms timer
void tmc5130_StartTimedConstantVelocity(struct TMC5130_t* instance, uint32_t acc, int32_t velocity, uint32_t timeInms, uint32_t ticks);

/// Moves the motor until a limit switch attached is triggered
/// \param instance a pointer to the instance struct
/// \param switchId Switch ID (Left = 0, Right = 1+)
/// \param polarity Polarity of the switch (Active High/Low)
/// \param acc Unsigned Acceleration Value
/// \param velocity Unsigned Velocity Max. Range +-(2^23-512)
void tmc5130_StartMoveUntilSwitch(struct TMC5130_t* instance, uint8_t switchId, bool polarity, uint32_t acc, int32_t velocity);

void tmc5130_startShaking(struct TMC5130_t* instance, int32_t initialshakespeed, int32_t shakeAmplitude, int32_t targetshaketime, float shakerIValue, uint32_t ticks);
void tmc5130_stopShaking(struct TMC5130_t* instance);
void tmc5130_stop(struct TMC5130_t* tmc5130);

/// Moves the motor to the desired position
/// \param instance a pointer to the instance struct
/// \param position Signed motor position. Range +-(2^31-1)
/// \param acc Unsigned Acceleration Value
/// \param speedMax Unsigned Velocity Max. Range 0...(2^23-512)
/// \param dec Unsigned Deacceleration Value
void tmc5130_moveTo(struct TMC5130_t* instance, int32_t position, uint32_t acc, uint32_t speedMax, uint32_t dec);

/// Sets the Idle current scaling factor
/// \param instance a pointer to the instance struct
/// \param idleCurrent a value between 0 and 31, where 31 is full scale current
/// \param moveCurrent a value between 0 and 31, where 31 is full scale current
void tmc5130_setDigitalCurrentControls(struct TMC5130_t* instance, uint8 idleCurrent, uint8_t moveCurrent, uint8_t moveCurrentShaker);

// Sets the Idle current scaling factor
/// \param instance a pointer to the instance struct
/// \param powerDownInmS sets the delay time after stand still of the motor to motor current power down. in mS.
void tmc5130_setDigitalCurrentPowerDown(struct TMC5130_t* instance, uint16_t powerDownInmS);

#ifdef __cplusplus
}
#endif

#endif /* TMC_IC_TMC5130_H_ */
